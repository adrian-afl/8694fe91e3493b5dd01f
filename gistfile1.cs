using System;

namespace ConsoleApplication4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            float test = 666.6666666f;
            DateTime now = DateTime.Now;
            Random rand = new Random();
            while (true)
            {
                float i = (float)rand.NextDouble();
                test = (test / i) * i;
                if ((DateTime.Now - now).TotalSeconds > 4) break;
            }
            Console.Write(test);
            Console.Read();
        }
    }
}